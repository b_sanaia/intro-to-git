export function isAuthorized() {
    let user = localStorage.getItem('logedInUser');
    const authContainer = document.getElementsByClassName('auth')[0];
    if (user) {
        user = JSON.parse(user);
        authContainer.innerHTML = '';
        const html = `
        <li>
            <span>${user.username}</span>
        </li>
        <li>
            <button class='btn-primary' id='logout'>logout</button>
        </li>
        `

        authContainer.insertAdjacentHTML('afterbegin', html);

        document.getElementById('logout').addEventListener('click', logout)
        return true;
    } else {
        const html = `
        <li>
            <button class='btn-primary login'>login</button>
        </li>
        <li>
            <button class='btn-primary register'>register</button>
        </li>
        `
        authContainer.insertAdjacentHTML('afterbegin', html);
        return false;
    }
}


function logout() {
    localStorage.removeItem('logedInUser');
    location.reload();
}