import { isAuthorized } from './check-logedin.js';
import { login } from './login.js';
import { register } from './register.js';

export function auth() {
    if (!isAuthorized()) {
        register();
        login();
    }
}