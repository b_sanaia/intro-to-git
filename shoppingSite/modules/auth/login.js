import { toggleModal } from "./auth-modal.js";

export function login() {
    const loginBtn = document.getElementsByClassName('login')[0];
    loginBtn.addEventListener('click', listener)

}


function listener() {
    toggleModal('auth-modal', 'close-login');
    const loginBtn = document.getElementById('authorize');
    loginBtn.addEventListener('click', () => {

        const username = document.getElementById('username').value;
        const password = document.getElementById('password').value;

        fetch('http://localhost:3000/users').then(res => {
            return res.json()
        }).then(users => {
            const user = users.filter(user => user.username == username && user.password == password);
            if (user.length > 0) {
                loginUser(username, password);
            } else {
                document.getElementById('error-message').style.display = 'block';
            }
        })
    })
}


export function loginUser(username, password) {

    fetch('http://localhost:3000/users').then(res => {
            return res.json()
        }).then(users => {
            const user = users.filter(user => user.username == username && user.password == password);
            if (user.length > 0) {
                localStorage.setItem('logedInUser', JSON.stringify(user[0]));
                location.reload();
            } else {
                document.getElementById('error-message').style.display = 'block';
            }
        })
}