import { toggleModal } from "./auth-modal.js";
import { loginUser } from "./login.js";


export function register() {
    const registerBtn = document.getElementsByClassName('register')[0];
    registerBtn.addEventListener('click', listener);
}

function listener() {
    const registerBtn = document.getElementById('register');
    toggleModal('register-modal', 'close-register', registerBtn, registerCallbackFunc);

    registerBtn.addEventListener('click', registerCallbackFunc);
}


function registerCallbackFunc() {
    const username = document.getElementById('register-username').value;
    const password = document.getElementById('register-password').value;
    const confirmPassword = document.getElementById('register-confirm-password').value;

    if (!username.trim() || !password.trim() || !confirmPassword.trim()) {
        return
    }

    if (password === confirmPassword) {
        const data = {username: username, password: password};
        fetch('http://localhost:3000/users').then(res => {
            return res.json()
        }).then(users => {
            if (users.filter(user => user.username == username).length == 0) {
                registerUser(data);
            } else {
                document.getElementById('register-error-message').style.display = 'block';
            }
        })
    }
}

function registerUser(data) {
    fetch('http://127.0.0.1:3000/users', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    }).then(() => {
        loginUser(data.username, data.password);
    });


}

