function hideModal(modal, backdrop, btn, callback) {
    modal.style.visibility = 'hidden';
    backdrop.style.visibility = 'hidden';
    // location.reload(); // hack
    btn.removeEventListener('click', callback);

}

function showModal(modal, backdrop) {
    modal.style.visibility = 'visible';
    backdrop.style.visibility = 'visible';
}

export function toggleModal(class1, class2, btn, callback) {
    const modal = document.getElementsByClassName(class1)[0];
        const backdrop = document.getElementsByClassName('backdrop')[0];
        const closeBtn = document.getElementById(class2);
        showModal(modal, backdrop)

        closeBtn.addEventListener('click', () => {
            hideModal(modal, backdrop, btn, callback);
        });

        backdrop.addEventListener('click', () => {
            hideModal(modal, backdrop, btn, callback);
        });
}
