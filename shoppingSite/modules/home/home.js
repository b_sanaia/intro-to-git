import { auth } from "../auth/main.js";

class Product {
    constructor(img, title, desc) {
        this.img = img;
        this.title = title;
        this.desc = desc;
    }

    draw() {
        let container = document.getElementsByClassName('suggestedProducts')[0];
        let html = `
        <div class="card me-2" >
            <img src="${this.img}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">${this.title}</h5>
                <p class="card-text">${this.desc}</p>
                <div class="product-inputs-container">
                    <input class="form-control me-3" type="number" min="0" placeholder="Quantity">
                    <a class="btn btn-primary">Buy</a>
                </div>
            </div>
        </div> 
        `
        container.insertAdjacentHTML('beforeend', html);
    }
}




window.onload = function() {
    const product1 = new Product('../assets/istockphoto-916092484-612x612.jpg', 'Product 1', "Some quick example text to build on the card title and make up the bulk of the card's content.");
    product1.draw();
    const product2 = new Product('../assets/istockphoto-916092484-612x612.jpg', 'Product 2', "Some quick example text to build on the card title and make up the bulk of the card's content.");
    product2.draw();
    const product3 = new Product('../assets/istockphoto-916092484-612x612.jpg', 'Product 3', "Some quick example text to build on the card title and make up the bulk of the card's content.");
    product3.draw();


    auth();
    
}
