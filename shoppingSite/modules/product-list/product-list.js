import { generatePagination } from "./pagination.js";
import { renderProducts } from "./render-products.js";
import { search } from "./search.js";
import { filter } from './filter.js';
import { auth } from '../auth/main.js';
import { addToCart } from "./add-to-cart.js";


export const ITEM_COUNT = 2;

function main() {

    const PRODUCTS = [
        {price: 11, id: 1, title: 'Product Title 1', desc: 'Some quick example text to build on the card 1', img: '../assets/istockphoto-916092484-612x612.jpg'},
        {price: 22, id: 2, title: 'Product Title 2', desc: 'Some quick example text to build on the card 2', img: '../assets/istockphoto-916092484-612x612.jpg'},
        {price: 33, id: 3, title: 'Product Title 3', desc: 'Some quick example text to build on the card 3', img: 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg'},
        {price: 44, id: 4, title: 'Product Title 4', desc: 'Some quick example text to build on the card 4', img: '../assets/istockphoto-916092484-612x612.jpg'},
        {price: 55, id: 5, title: 'Product Title 5', desc: 'Some quick example text to build on the card 5', img: '../assets/istockphoto-916092484-612x612.jpg'},
        {price: 66, id: 6, title: 'Product Title 5', desc: 'Some quick example text to build on the card 5', img: '../assets/istockphoto-916092484-612x612.jpg'},
        {price: 77, id: 7, title: 'Product Title 5', desc: 'Some quick example text to build on the card 5', img: '../assets/istockphoto-916092484-612x612.jpg'},
    ];

    renderProducts(PRODUCTS, 0, ITEM_COUNT);
    generatePagination(PRODUCTS, ITEM_COUNT);
    search(PRODUCTS);
    filter(PRODUCTS);
    auth();
    document.getElementsByClassName('cart-icon')[0].addEventListener('click', () => {
        location.href = 'http://127.0.0.1:8080/cart.html'
    })    
}

window.addEventListener('load', () => {
    main();
});