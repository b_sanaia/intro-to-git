import { generatePagination } from "./pagination.js";
import { ITEM_COUNT } from "./product-list.js";
import { renderProducts } from "./render-products.js";

export function search(products) {
   const searchInp =  document.getElementById('search');
   searchInp.addEventListener('keyup', (ev) => {
       const searchValue = searchInp.value;
       const filtered = products.filter(p => p.title.toLowerCase().includes(searchValue.toLowerCase()));
       renderProducts(filtered, 0, ITEM_COUNT);
       generatePagination(filtered, ITEM_COUNT);
   });
}