import { ITEM_COUNT } from "./product-list.js";
import { renderProducts } from './render-products.js';
import { generatePagination } from "./pagination.js";

const sidebar = document.getElementsByClassName('sidebar')[0];
const backdrop = document.getElementsByClassName('sidebar-backdrop')[0];


export function filter(products) {
    const filterBtn =  document.getElementById('filter-btn');
    filterBtn.addEventListener('click', () => {
        showSidebar();
        addBacdkropListener();
        addFilterListener(products);
    })
}

function showSidebar() {
    sidebar.classList.remove('hidden');
    sidebar.classList.add('show');

    backdrop.style.visibility = 'visible';
    backdrop.style.opacity = '1';
}

function hideSidebar() {
    sidebar.classList.add('hidden');
    sidebar.classList.remove('show');

    backdrop.style.visibility = 'hidden';
    backdrop.style.opacity = '0';
}

function addBacdkropListener() {
    backdrop.addEventListener('click', () => {
        hideSidebar();
    })
}


function addFilterListener(products) {
    const filter = document.getElementById('filter');
    const from = document.getElementById('from');
    const to = document.getElementById('to');
    filter.addEventListener('click', () => {
        filterProds(products, from.value, to.value)
    });

    document.getElementById('from').addEventListener('keypress', function (e) {
        if (e.key === 'Enter') {
            filterProds(products, from.value, to.value)
        }
    });

    document.getElementById('to').addEventListener('keypress', function (e) {
        if (e.key === 'Enter') {
            filterProds(products, from.value, to.value)
        }
    });
}

function filterProds(products, from ,to) {
    let fitleredProducts;
    if (from && !to) {
        fitleredProducts = products.filter(pr => {
            return pr.price >= from;
        });
    } else if (!from && to) {
        fitleredProducts = products.filter(pr => {
            return pr.price <= to;
        });
    } else {
        fitleredProducts = products.filter(pr => {
            return pr.price >= from && pr.price <= to;
        });
    }
    renderProducts(fitleredProducts, 0, ITEM_COUNT);
    generatePagination(fitleredProducts, ITEM_COUNT);
    hideSidebar();
}
