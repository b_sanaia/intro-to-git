import { addToCart } from "./add-to-cart.js";

export function renderProducts(products, from, to) {
    const productList = document.getElementsByClassName('product-list')[0];
    productList.innerHTML = '';
    let productsHTML = '';
    products.slice(from, to).forEach(product => {
        productsHTML += `
        <div class="card">
            <img src="${product.img}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">${product.title}</h5>
                <p class="card-text">${product.desc}</p>
                <div class='d-flex align-items-center justify-content-between'> 
                    <a class="btn btn-primary add-to-cart" id='${product.id}'>Add to cart</a>
                    <span>Price: ${product.price}$ </span>
                </div>
            </div>
        </div>
        `
    });
    productList.insertAdjacentHTML('beforeend', productsHTML);
    addToCart(products);

}