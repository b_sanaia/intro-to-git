
const cartContainer = document.getElementsByClassName('cart-list')[0];

export function renderCartList() {
    const products = JSON.parse(localStorage.getItem('products'));
    if (products) {
        cartContainer.innerHTML = '';
        const productsToRender = [];
        products.forEach(product => {
            if (productsToRender.filter(p => p[0].id == product.id).length > 0) {
                productsToRender.map(p => {
                    if (p[0].id == product.id) {
                        p[1] += 1;
                    }
                });
            } else {
                productsToRender.push([product, 1])
            }
        });

        render(productsToRender)
        
    } else {
        cartContainer.innerHTML = 'Cart is empty!'
    }
}


function render(prods) {
    let html = '';
    console.log(prods);
    prods.forEach(p => {
        html += `
        <div style="box-shadow: 0px 2px 5px rgb(100 100 100);" class="cart-item my-4 mx-4 d-flex align-items-center justify-content-between">
            <div class="d-flex align-items-center">
                <img class="cart-img" src="${p[0].img}" alt="">
                <span class='mx-3'>${p[0].title}</span>
                <span class="mx-3">Count ${p[1]}</span>
            </div>
        </div>  
        `
    })
    cartContainer.insertAdjacentHTML('beforeend', html);
    
}