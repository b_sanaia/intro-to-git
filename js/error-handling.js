// console.log(1);
// throw new Error("you have error");
// console.log(2);


// throw new SyntaxError('you have syntax error');


// try {
//     throw new Error('you have error');
// } catch(err) {
//     // console.log(err);
//     // console.log(err.name);
//     // console.log(err.message);
//     console.log(err.stack);
// }


// try {
//     throw new Error('important');
// } catch(err) {
//     if (err.message === 'important') {
//         throw err;
//     } else {
//         console.log(err.message);
//     }
// }



// function dispatcher(status, message) {
//     if (status === 'warning') {
//         throw new Error('warning')
//     } else if (status === '911') {
//         throw new Error('911')
//     } else if (status === 'inform') {
//         console.log(message);
//     }
// }


// try {
//     dispatcher('warning', 'Some information');
// } catch(err) {
//     if (err.message === '911') {
//         throw err;
//     } else {
//         console.log("WARNING: " + err.message + '!')
//     }
// }




// try {
//     console.log('try');
//     throw new Error('warning');
// } catch(err) {
//     console.log('catch');
//     console.log(err.message);
// } finally {
//     console.log('finally')
// }



// function add(a, b) {
//     try {
//         console.log('1');
//         return a + b;
//         console.log('2');
//     } finally {
//         console.log('function was closed');
//     }
// }

// let result = add(3, 5);
// console.log(result);



