window.addEventListener('load', () => {



    // WAY 1
    // const elements = [...document.getElementsByClassName('demo')] // returns array
    // for (let el of elements) {
    //     console.log(el);
    // }

    // elements.forEach((el, idx) => {
    //     // el.textContent = `Demo ${idx + 1}`
    //     // el.textContent = `<b> Demo ${idx + 1} </b>`;
    //     // el.innerHTML = `<b> Demo ${idx + 1} </b>`;
    // })


    // WAY 2
    // const elements = [...document.getElementsByClassName('demo')];
    // elements[0].style.color = 'red';

    // const firstElement = document.getElementById('test');
    // firstElement.style.opacity = '0';

    // Way 3


    // const elements = [...document.getElementsByTagName('p')];
    // console.log(elements)


    // WAY 4
    
    // const element = document.querySelector('.demo.element');
    // console.log(element);

    // const elements = document.querySelectorAll('.demo');
    // console.log(elements);

    // const first = document.querySelector('#test');
    // console.log(first);


    // EVENTS

    // function clicked() {
    //     alert('element clicked')
    // }


    // const el = document.getElementById('test');
    // el.addEventListener('click', () => {
    //     alert('el clicked')
    // })
    // https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg
    // const el = document.getElementById('test');
    // el.onclick = () => {
    //     document.getElementById('img').src = "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg";
    //     el.style.color = 'green';
    //     el.innerHTML = 'Image Loaded'
    // }


    // const el = document.getElementById('test');
    // el.style.transition = 'all .3s'
    // // el.onmouseover = () => {
    // //     el.style.color = 'green';
    // // }

    // el.onmouseenter = () => {
    //     el.style.color = 'green';
    // }

    // el.onmouseleave = () => {
    //     el.style.color = 'red';
    // }


    // INPUT LISTENERS
    // const input = document.getElementById('inp');
    // input.addEventListener('keyup', (e) => {

    //     // if (!((e.keyCode >= 65 && e.keyCode <= 90) ||
    //     //     (e.keyCode >= 97 && e.keyCode <= 122))) {
    //     //         e.preventDefault();
    //     // }

    //     console.log(e);
    //     document.getElementById('key').innerText = e.key;
    //     document.getElementById('text').innerHTML = input.value;
    // })


    // const input = '<input type="text">';
    // const paragraph = '<p> test paragraph </p>';

    // const body = document.getElementsByTagName('body')[0];
    // // body.appendChild(input);
    // body.innerHTML += input;
    // body.innerHTML += paragraph;

    // document.body.appendChild(input);
    // document.body.appendChild(paragraph);

    // document.createElement()


})


// function clicked() {
//     alert('element clicked')
// }