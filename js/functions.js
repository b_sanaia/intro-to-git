// function type void


// add();
// way 1
// function add() {
//     console.log('asdfaf');
// }

// way 2
// let add = function() {
//     console.log('asdfaf')
// }

// add()


// function add(a, b) {
//     return a + b;
// }

// console.log(add(324534, 345345));
// console.log(x);
// let y = add(11, 12);
// console.log(x);


// function hello() {
//     console.log('hello');
//     console.log('hello');
//     console.log('hello');
//     console.log('hello');
//     console.log('hello');
// }

// hello();


// imeddiate invoked functions
// (function() {
//     console.log('heelo world')
// })()



// Recursion
// function x(i) {
//     console.log('hello world');
//     if (i !== 11) {
//         i += 1;
//         x(i);
//     }
// }

// x(0);



// Arrow functions

// const func = n => n;

// let result = func(12);
// console.log(result);


// const add = (a, b) => a + b;
// let result = add(12, 24);
// console.log(result);


// const func = () => 'string'
// console.log(func())


// const add = (a, b) => {
//     const sum = a + b;
//     return sum;
// }

// console.log(add(1, 2));
// console.log(typeof add);



// Function inside function

// let f = function() {
//     return function(x) {
//         console.log(x)
//     }
// }

// const func = f();
// func('sdfgfsdf');



// function f(x) {
//     let double = x * 2;

//     return function(y) {
//         return double + y;
//     }
// }

// console.log(f(2)(3));


// Function arguments
// function f() {
//     console.log(arguments);
//     console.log(arguments[0]);
// }


// f(1, 2, 3, 4)


// function f() {
//     for (item of arguments) {
//         console.log(item);
//     }
// }


// f(1, 2, 3, 4)


