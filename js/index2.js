// let x = 5;
// ==
// ===
// let x = 5; // number
// let y = '5'; // string

// ტიპებს არ ადარებს, ადარებს მნიშვნელობებს
// console.log(x == y)

// ადარებს ტიპებს, ადარებს მნიშვნელობებს
// console.log(x === y)


// let x = 10;
// let y = 10;
// ტიპებს არ ადარებს, ადარებს მნიშვნელობებს
// console.log( x != y)

// ადარებს ტიპებს, ადარებს მნიშვნელობებს
// console.log( x !== y)


// let x = 10;
// let y = 15;
// console.log(x > y);
// console.log(x < y);
// console.log(x >= y);
// console.log(x <= y);


// let x = 10;
// let y = 15;

// console.log(x > 20 && y > x)
// false and true = false

// console.log(x > 20 || y > x)
// false or true = true
// false or false = false


// let x = -3;

// if (x > 0) {
//     console.log('Number is positive');
// } else if (x < 0) {
//     console.log('Number is negative');
// } else {
//     console.log('Number is zero');
// }

// if (x > 0) {
//     console.log('Number is positive');
// } else if (x === -1) {
//     console.log('Number is -1');
// } else if (x === -2) {
//     console.log('Number is -2');
// } else if (x === -3) {
//     console.log('Number is -3');
// } else if (x === -4) {
//     console.log('Number is -4');
// } else {
//     console.log('Number is below -4');
// }



// Teenager program
// const YEAR = 2022;
// const birthday = 2005;
// if ( YEAR - birthday <= 19 && YEAR - birthday >= 13 ) {
//     console.log('You are teenager')
// } else {
//     console.log('You are not teenager')
// }



// const DAY = 1;

// if (DAY === 1) {
//     console.log('monday')
// } else if (DAY === 2) {

// }
// // ...
// else {
//     console.log('sunday')
// }

// switch (DAY) {
//     case 1:
//         console.log('Monday');
//         break;
//     case 2:
//         console.log('Tuesday');
//         break;
// }


// Ternary operator
// let x = 5;

// let y = '';
// if (x > 0) {
//     y = "More then 0";
// } else {
//     y = "Less then 0";
// }

// Aleternative
// let y = x > 0 ? "More then 0" : x < -10 ? 'dsf' : 'sdf';
// console.log(y);