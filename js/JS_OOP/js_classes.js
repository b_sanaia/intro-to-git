// function PersonType(name) {
//     this.name = name;
// }

// PersonType.prototype.sayName = function() {
//     console.log(this.name);
// }


// const person = new PersonType('Beqa');
// person.sayName();


// class PersonClass {
//     constructor(name) {
//         this.name = name;
//     }

//     sayName() {
//         console.log(this.name);
//     } // this
// }


// const beqa = new PersonClass('Beqa');
// const lado = new PersonClass('Lado');
// beqa.sayName();
// console.log(beqa.name);
// console.log(beqa instanceof PersonClass);
// console.log(beqa instanceof Object);
// console.log(typeof PersonClass);



// class CustomClass {
//     constructor(x) {
//         this.x = x;
//     }

//     get getMethod() {
//         return 'sdfsdfsdf';
//     }

//     set setMethod(q) {
//         this.x = q;
//         return q + 'dsfsdf';
//     }
// }


// const instance = new CustomClass('test');
// instance.setMethod = 'asd';
// console.log(instance.x);



// methodName = 'sayName'

// class TestClass {
//     constructor(name) {
//         this.name = name;
//     }

//     [methodName]() {
//         console.log(this.name);
//     }

// }

// const instance = new TestClass('Beqa');
// instance.sayName()


// Static method
// class Person {
//     constructor(name) {
//         this.name = name;
//     }

//     sayName() {
//         console.log(this.name);
//     }

//     static create(name) {
//         return new Person(name);
//     }
// }


// const instance = new Person('Beqa');
// const newInstance = Person.create('Gigo'); // new Person('Gigo')

// instance.sayName();
// newInstance.sayName();



// Inheritence

// class Core {
//     constructor(name) {
//         this.name = name;
//     }
// }


// class Person extends Core {
//     sayName() {
//         console.log(`person's name is ${this.name}`);
//     }
// }

// class Car extends Core {

//     getCarBrand() {
//         console.log(`car brand is ${this.name}`);
//     }
// }


// const instance = new Person('Beqa');
// instance.sayName();

// const car = new Car('BMW')
// car.getCarBrand();




// Super in constructor

// class Core {
//     constructor(name) {
//         this.name = name;
//     }
// }


// class Car extends Core {
//     constructor(name) {
//         super(name)
//     }

//     get carBrand() {
//         return `Car brand is: ${this.name}`
//     }
// }

// const car = new Car('BMW');
// console.log(car.carBrand);



// class Rectangle {
//     constructor(width, length) {
//         this.width = width;
//         this.length = length;
//     }

//     getArea() {
//         return this.width * this.length;
//     }
// }


// class Square extends Rectangle {
//     constructor(width) {
//         super(width, width);
//     }

//     getArea() {
//         return `Square Area is ${super.getArea()}`
//     }
// }


// const square = new Square(10);
// console.log(square.getArea());






// class TestClass {

//     #password = '123456';

//     constructor() { }

//     get password() {
//         return this.#password;
//     }

//     set password(value) {
//         throw new Error('You cant change password')
//     }
// }


// const instance = new TestClass();
// // console.log(instance.#password) // error
// console.log(instance.password) // error
// instance.password = 'sdfgsdfgnfdjk'
