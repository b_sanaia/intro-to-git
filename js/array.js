// let arr = ['ssjhdfjhsdfb', 1, true];
// console.log(arr)


// let arr = new Array(1, 2, 3)
// console.log(arr)

// let fruits = ['vashli', 'msxali', 'atami'];
// console.log(fruits.length)
// console.log(fruits[2])
// fruits[0] = 'alubali';
// console.log(fruits);
// console.log(fruits[5]) // undefined;
// fruits[5] = 'portoxali';
// console.log(fruits);
// console.log(fruits.length);
// console.log(typeof fruits)
// console.log(Array.isArray(fruits));
// console.log(Array.isArray('asd'));



// let x = [1, 2, 3];
// let y = x;
// console.log(x == y);
// console.log(x === y);
// console.log('----------------------');
// console.log('x array', x);
// console.log('y array', y);
// console.log('----------------------');
// y[0] = 5;
// console.log('x array', x);
// console.log('y array', y);


// let arr = Array.of(1, 2, 3);
// let arr = Array.of([1, 2, 3], 'string', 34);
// console.log(arr);


// let arr = Array.from([1, 2, 3]);
// let arr = Array.from('string');
// console.log(arr);


// Callback function
// let arr = Array.from([2, 3, 4], function(arrayItem) {
//     return arrayItem * 10
// });

// console.log(arr);


// const str = 'My name is Beqa';
// console.log(str.split(' '));



// array loop methods
// let arr = [1, 2, 'test', 4];
// for (let i = 0; i < arr.length; i++) {
//     console.log(arr[i])
// }

// Indexes
// for (let i in arr) {
//     console.log(i)
// }

// Indexes
// for (let item of arr) {
//     console.log(item)
// }

// let arr = [1, 2, 'test', 4];
// arr.forEach(function(item, i, arr) {
//     // item = item * 5
//     console.log(item, i, arr)
// })
// console.log(arr)


// let arr = [1, -2, 3, -4, 5];
// let positiveElemArray = arr.filter(function(item) {
//     return item > 0
// });

// console.log(positiveElemArray);


// let arr = [2, 3, 4, 5, 6];
// arr.map(function(item) {
//     return item * 15;
// });
// console.log(arr)
// let multiplied = arr.map(function(item) {
//     return item * 15;
// });
// console.log(multiplied);


// function reduceFunc(sum, current) {
//     return sum * current
// }


// let arr = [2, 3, 4, 5, 6];
// let transformed = arr.reduce(reduceFunc);
// console.log(transformed);


// let arr = ['a', 'b', 'c'];
// let transformed = arr.reduce(function(sum, current) {
//     return sum + current
// }, '1234')
// console.log(transformed)


// let arr = ['a', 'b', 'c'];
// let transformed = arr.reduceRight(function(sum, current) {
//     return sum + current
// }, '')
// console.log(transformed)


// let s = 'test string';
// let arr = s.split(' ');
// console.log(arr);
// console.log(arr.join('****'))


// let arr = [1, -2, 3, -4, 5];
// let x = arr.every(function(item) {
//     return item > 0;
// });

// console.log(x);

// let arr = [1, -2, 3, -4, 5];
// let x = arr.some(function(item) {
//     return item > 0;
// });
// console.log(x);


// let arr = [5, 3, 11, 54, 0];
// console.log(arr.sort(function(a, b) {
//     console.log(a, b)
//     return a - b
// }))


// let arr = ['a', 1, 2];
// // arr[0] = 'asd';
// arr.push('sdfsdf');
// console.log(arr);

// const poped = arr.pop();
// console.log(arr)
// arr.pop();
// console.log(arr);

// console.log(poped);


// function func(a, n) {
//     let arr = [];
//     for (let i = 0; i < n; i++) {
//         arr.push(a)
//     }
//     console.log(arr)
// }

// func('a', 10);
// ['a', 'a','a','a','a','a','a','a','a','a'];


// let arr = [ [1, 2, 3], [4, 5, 6], [7, 8, 9] ];
// arr.forEach(function(a) {
//     a.forEach(function(item) {
//         console.log(item);
//     })
// })


// Non mutation methods
// let a = [1, 2, 3];
// let b = [4, 5, 6];
// // let c = a.concat(b);
// let c = b.concat(a);
// console.log(c);
// console.log(a);
// console.log(b);


// let a = [1, 2, 3];
// let b = ['a', 'b', 'c'];
// let c = a.concat(b);
// console.log(c);

// let a = [1, 2, 3];
// let d = [4, 5, 6];
// let b = ['a', 'b', 'c'];
// let c = a.concat(b, d);
// console.log(c)


// let a = [1, 2, 3, [4, 5, 6]];
// let b = a.flat(); // Default 1
// console.log(a);
// console.log(b);

// let a = [1, 2, 3, [4, 5, 6, [23, 43, 65, [1213, 123, 312]] ] ];
// let b = a.flat(Infinity); 
// console.log(b)


// let a = [1, 2, 3];
// let b = a.map(function(item) {
//     return [item * 3];
// });

// let a = [1, 2, 3];
// let b = a.flatMap(function(item) {
//     return [item * 3];
// });

// console.log(b);


// let a = [1, 2, 3, 4];
// let b = a.slice(1, 3); // 3 არ შედის, end ინდექსი არ შედის
// console.log(b)



// Array search methods
// let a = [1, 2, 3, 4];
// let b = a.find(function(item) {
//     return item > 2;
// })

// console.log(b);


// let a = [1, 2, 3, 4];
// let b = a.filter(function(item) {
//     return item > 3;
// })

// console.log(b);


// let a = [1, 2, 3, 4];
// let b = a.findIndex(function(item) {
//     return item > 2;
// })

// console.log(b);


// let a = [1, 2, 'a', 4, 3];
// let b = a.indexOf(3)
// console.log(b);

// let a = [1, 2, 3, 4, 3];
// let b = a.lastIndexOf(3)
// console.log(b);

// function findIndexes(a, arr) {
//     let indexes = [];
//     arr.forEach(function(item, i){
//         if (item === a) {
//             indexes.push(i)
//         }
//     });

//     console.log(indexes);
// }


// let arr = [1, 2, 3, 4, 3, 2, 2];
// findIndexes(3, arr);
// findIndexes(2, arr);