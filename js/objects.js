// let person = { name: 'Beqa' } // key: value

// console.log(person);

// let person = {};
// person.name = 'Beqa';


// let person = {};
// person.name = 'Beqa';
// console.log(person);
// person.name = 'Nika';
// console.log(person);
// console.log(typeof person);



// let person = { name: 'Beqa', name: "234234" } // key: value

// console.log(person);
// console.log(person.name);
// console.log(person['name']);
// let key = 'name';
// console.log(person[key])
// console.log(person['n' + 'a' + 'm' + 'e']);
// function getKey() {
//     return 'name'
// }

// console.log(person[getKey()])

// console.log(person)



// const o = {};
// const n = o.person.name;
// console.log(n);


// const data = { type: 1 };
// console.log(data && data.type);

// const title = data && data.type && data.type.title;
// console.log(title);



// Functions in objects (Object methods)
// const person = {
//     getAge: function() {
//         return 24;
//     }
// }

// console.log(person.getAge());

// const person = {};
// person.getAge = function() {
//     console.log(24);
// }

// person.getAge();

// const person = {
//     getAge: () => {
//         return 24;
//     }
// }

// console.log(person.getAge());

'use strict';

// let person = {};
// Object.defineProperty(person, 'name', {
//     value: "Beqa",
//     writable: false,
//     configurable: false
// });


// person.name = "Giorgi";
// console.log(person.name);
// delete person.name;

// console.log(Object.getOwnPropertyDescriptors(person));

// const person = {
//     name: "Beqa",
//     getName() {
//         console.log(this)
//         return this.name;
//     }
// }

// Object.defineProperty(person, 'getName', {
//     enumerable: false
// })

// for (const key in person) {
//     console.log(key);
// }
// console.log(person.getName())

// console.log(Object.getOwnPropertyDescriptors(person));



// const o = {
//     a: 1,
//     b: 2,
//     c: 3
// }

// Object.defineProperty(o, 'c', {
//     enumerable: false
// });

// for (let item in o) {
//     console.log(item)
// }

// console.log(Object.keys(o));
// console.log(Object.getOwnPropertyNames(o));


// Getters and setters
// const person = {
//     firstName: "Beqa",
//     lastName: "Sanaia",
// }


// Object.defineProperty(person, 'fullName', {
//     get() {
//         console.log(this);
//         return this.firstName + " " + this.lastName;
//     }
// });

// console.log(person.fullName);


// const user = {
//     firstName: null,
//     lastName: null,
// }

// Object.defineProperty(user, 'fullName', {
//     get() {
//         return this.firstName + " " + this.lastName;
//     },
//     set(value) {
//         let splittedValue = value.split(' ');
//         this.firstName = splittedValue[0];
//         this.lastName = splittedValue[1];
//     }
// })

// console.log(user.fullName);
// console.log(user.lastName);
// console.log(user.firstName);

// user.fullName = 'Beqa Sanaia';
// console.log(user.fullName);
// console.log(user.lastName);
// console.log(user.firstName);


// const user = {
//     firstName: null,
//     lastName: null,

//     get fullName() {
//         return this.firstName + " " + this.lastName;
//     },

//     set fullName(value) {
//         let splittedValue = value.split(' ');
//         this.firstName = splittedValue[0];
//         this.lastName = splittedValue[1];
//     }
// }


// user.fullName = 'Beqa Sanaia';
// console.log(user);

// console.log(user.fullName);

const user = {}

Object.defineProperties(user, {
    firstName: {
        value: 'Beqa',
    },
    lastName: {
        value: "Sanaia"
    },
    fullName: {
        get: function() {
            return this.firstName + ' ' + this.lastName
        }
    }
})


console.log(user.fullName);