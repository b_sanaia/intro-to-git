
// *********OBJECT DESTRUCTURING*************
// let obj = {
//     result: true,
//     saved: false
// }

// let a = obj.result;
// let b = obj.saved;

// console.log(obj.result);
// console.log(obj.saved);

// let { result, saved } = obj;
// console.log(result);
// console.log(saved);


//------------------------------
// let obj = {
//     result: true,
//     saved: false
// }

// let { x, saved } = obj;
// console.log(x);
// console.log(saved);

//- ---------------------------------

// let obj = {
//     result: true,
//     saved: false
// }

// let { x = 'string' } = obj;
// console.log(x);

// console.log(Object.keys(obj))


//----------------------------------

// let obj = {
//     result: true,
//     saved: false
// }

// let { saved: x } = obj;
// console.log(x);



//----------------------------------

// let obj = {
//     result: true,
//     saved: false
// }

// let { saved: x = true } = obj;
// console.log(x);


//----------------------------------

// let obj = {
//     result: true,
//     saved: false,
//     user: {
//         name: "Beqa",
//         age: {
//             x: 24
//         }
//     }
// }


// let { user: { name, age: { x, y = 'test' } } } = obj;
// // console.log(user);
// console.log(name);
// console.log(x);
// console.log(y);


//----------------------------------

// let obj = {
//     result: true,
//     saved: false,
//     user: {
//         name: {
//             en: 'Beqa',
//             ka: 'ბექა'
//         }
//     }
// }

// let { user: { name: { ka } } } = obj;
// console.log(ka)



// *********ARRAY DESTRUCTURING************

// let arr = [1, 2, 3];
// let [first, second, third] = arr;
// console.log(first);
// console.log(second);
// console.log(third);

// let first = arr[0];
// let second = arr[1];
// let third = arr[2];

// -----------------------------------
// let arr = [1, 2, 3];
// let [second, first, third] = arr;
// console.log(first);
// console.log(second);
// console.log(third);

// -----------------------------------
// let arr = [1, 2, 3];
// let [,,third] = arr;
// console.log(third);

// -----------------------------------
// let arr = [1, 2, 3];
// let [,,,third] = arr;
// console.log(third);

// -----------------------------------
// let arr = [1, 2, 3];
// let [,,,fourth='default'] = arr;
// console.log(fourth);

// -----------------------------------
// let arr = [1, 2, 3];
// let first = 'x';
// let second = 'y';

// first = 'asdsddbsfhgjedg'
// [first, second] = arr;
// console.log(first);
// console.log(second);


// -----------------------------------
// let arr = [1, [2, [3, 4]]];
// let [,[,[,x]]] = arr;
// console.log(x);


// -----------------------------------
// let user = {
//     name: {
//         first: 'Beqa',
//         last: "Sanaia"
//     },
//     age: 24,
//     cars: ['mercedes', 'audi']
// }

// let { name: { first: firstName, last: lastName }, cars: [firstCar, secondCar] } = user;

// console.log(`firstname is: ${firstName}, lastName is ${lastName}. first car is: ${firstCar} and second car is ${secondCar}`)

// console.log(firstName);
// console.log(lastName);
// console.log(firstCar);
// console.log(secondCar);


// *********STRING DESTRUCTURING************
// let [first, second] = 'string';
// console.log(first);
// console.log(second);




// *********PARAMETER DESTRUCTURING************
// const func = (name, value, { secure, expires }) => {
//     // option = option || {};


//     console.log(name);
//     console.log(value);
//     console.log(secure);
//     console.log(expires);
// }


// func('type', 'js', {
//     secure: 'true',
//     expires: 3000
// })


// *********SPREAD AND REST************
// let obj1 = {
//     a: 1,
//     b: 2
// }

// let obj2 = {
//     c: 3,
//     d: 4
// }

// let mergedObjs = { ...obj1, ...obj2 };
// console.log(mergedObjs);


// ------------------------------------------

// let arr1 = [1, 2, 3];
// let arr2 = [4, 5, 6];
// console.log([...arr1, ...arr2]);


// ------------------------------------------

// let arr = [1, 2, 3, 4, 5, 6];
// let [a,b, ...rest] = arr;
// console.log(a);
// console.log(b);
// console.log(rest);


// ------------------------------------------
// let user = {
//     name: "Beqa",
//     age: 24,
//     address: {
//         city: "Tbilisi",
//         country: "Georgia"
//     }
// }

// let { name: firstName, ...rest } = user;
// console.log(firstName);
// console.log(rest);


// ------------------------------------------
// function func(a, b, ...restParams) {
//     console.log(a);
//     console.log(b);
//     console.log(restParams);
// }

// func(1, 2, 5, 6, 7, 8, 9, 9, 54, 64);



let obj = {
    x: {
        y: 5
    },
    arr: ['string 1', 'string 2']
}

let { x: { y }, arr: [first, second] } = obj;

console.log(y);
console.log(first);
console.log(second);