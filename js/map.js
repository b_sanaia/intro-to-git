{
    const map = new Map([ [1, 1], ['2', 3] ])
    console.log(map);
}

{
    let calculator = {
        add: (a, b) => a + b,
        minus: (a, b) => a - b
    }

    const map = new Map();
    map.set(calculator.add, 'Calculator add function');
    map.set(calculator.minus, 'Calculator minus function');

    // map.delete(calculator.add);

    function getFunctionDescription(func) {
        return map.get(func)
    }

    console.log(getFunctionDescription(calculator.add))
    
}


{
    let obj = {a: 1};
    const weakSet = new WeakSet();
    weakSet.add(obj);
    console.log(obj);
}



{
    let obj = {a: 1};
    const weakMap = new WeakMap();
    weakMap.set(obj, {b: 2});
    console.log(weakMap);
}
