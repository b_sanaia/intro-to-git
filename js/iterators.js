// iterable and iterator protocol

// {value: 'next', done: false};


// const x = [1, 2, 3, 4];

// for (let y of x) {
//     console.log(y);
// }

// [Symbol.iterator]

// const arr = [1, 23, 3];
// const iterator = arr[Symbol.iterator]();
// console.log(iterator);
// console.log(iterator.next().value);
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());



// check if iterable

// const isIterable = obj => {
//     return typeof obj[Symbol.iterator] === 'function';
// }

// console.log(isIterable([1, 2, 3]));
// console.log(isIterable({}));
// console.log(isIterable(5));
// console.log(isIterable(new Set()));
// console.log(isIterable(new Map()));
// console.log(isIterable(function() {}));
// console.log(isIterable('string'));
// console.log(isIterable(new WeakMap()));
// console.log(isIterable({name: "Beqa"}));



// console.log(typeof function() {} === 'function')



// const obj = {
//     items1: [],
//     name: "Beqa",
//     age: 24,
//     items2: [4, 5, 6, 7, 8],
//     *[Symbol.iterator]() {
//         for (let item of [...this.items1, ...this.items2]) {
//             yield item;
//         }
//     }
// }

// obj.items1.push(11);
// obj.items1.push(12);
// obj.items1.push(13);


// for (let x of obj) {
//     console.log(x); 
// }



// const x = ['test1', 'test2', 'test3', 'test4'];
// for (const i of x.entries()) {
//     console.log(i);
// }

// const keys = [...x.keys()];
// console.log(keys);



// const set = new Set([11, 12, 13]);
// for (const item of set.entries()) {
//     console.log(item);
// }


// const map = new Map();
// map.set('name', 'Beqa');
// map.set('age', '24');
// for (let [x, y] of map.entries()) {
//     console.log('key: ', x, 'value: ', y);
// }
// console.log(map);


// const arr = [1, 2, 3];
// for (let x of arr.values()) {
//     console.log(x);
// }


// const arr = [...new Array(1000).keys()].filter(x => x % 2 == 0);
// console.log(arr);



// const map = new Map();
// map.set('name', 'Beqa');
// map.set('age', '24');

// for (const [x, y] of map) {
//     console.log(`${x}: ${y}`);
// }



// function createIterator(items) {
//     let i = 0;

//     return {
//         next() {
//             const done = i >= items.length;
//             const value = !done ? items[i++] : undefined;

//             return { done, value }
//         }
//     }
// }


// const iterator = createIterator([3, 4, 5, 6]);
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());



// function* createIterator() {
//     yield 1;
//     yield 2;
//     yield 3;
// }

// const iterator = createIterator();
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());



// function* createIterator(items) {
//     for (let item of items) {
//         yield item;
//     }
// }


// const iterator = createIterator([1,2,3,4,5,6,7,8,9,9,6,5,45,34,34]);
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());



// const createIterator = function*() {

// }



// const obj = {
//     createIterator: function*() {
//         yield 1;
//         yield 2;
//         yield 3;
//     }
// }

// const obj = {
//     *createIterator() {
//         yield 1;
//         yield 2;
//         yield 3;
//     }
// }

// const iterator = obj.createIterator();
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());



// function* createIterator() {
//     const first = yield + 2;
//     const second = yield first + 2;
//     yield second + 3
// }


// const iterator = createIterator();

// console.log(iterator.next());
// console.log(iterator.next(3));
// console.log(iterator.next(4));



// function* createIterator() {
//     yield 1;
//     return 5; // stops
//     yield 6;
// }



// function* createIterator() {
//     yield 1;
//     if (1 < 2) {
//         yield 2;
//     } else {
//         yield 3;
//     }
// }


// const iterator = createIterator();
// console.log(iterator.next())
// console.log(iterator.next())
// console.log(iterator.next())
// console.log(iterator.next())



// function* createNumberIterator() {
//     yield 1;
//     yield 2;
// }

// function* createStringIterator() {
//     yield 'Jon';
//     yield 'Anna';
// }

// function* createCombinedIterator() {
//     yield* createNumberIterator();
//     yield* createStringIterator();
//     yield true;
// }


// const iterator = createCombinedIterator();
// console.log(iterator.next())
// console.log(iterator.next())
// console.log(iterator.next())
// console.log(iterator.next())
// console.log(iterator.next())
// console.log(iterator.next())
// console.log(iterator.next())
// console.log(iterator.next())
// console.log(iterator.next())



// function* generateId() {
//     let id = 1;
//     while (true) {
//         const increment = yield id;
//         if (increment) {
//             id += increment
//         } else {
//             id++;
//         }
//     }

// }

// const genereted = generateId()

// console.log(genereted.next());
// console.log(genereted.next());
// console.log(genereted.next());
// console.log(genereted.next(5));
// console.log(genereted.next());
// console.log(genereted.next(5));
// console.log(genereted.next());



// const string = 'Test string';
// const iterator = string[Symbol.iterator]();
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());





// let obj = {
//     name: "test",
//     lastName: 'testtest',
//     x: 324
// }

// for (let x in obj) {
//     console.log(obj.x);
// }


// const o = {
//     age: 24.
// }

// console.log(o.age);
// console.log(o['age'])


// let obj = {
//     name: "test",
//     lastName: 'testtest',
//     x: 324,
//     *[Symbol.iterator]() {
//         for (let i = 1; i <= 100; i++) {
//             yield i;
//         }
//     }
// }


// for (let x of obj) {
//     console.log(x);
// }


// const arr = [...new Array(500).keys()].filter(arrayItem => arrayItem % 2 !== 0);
// console.log(arr);



