
// let s = 'strinG string';

// Indexed
// console.log(s[5])

// s t r i n g
// 0 1 2 3 4 5

//String size
// console.log(s.length)

// const idx = s.length - 1

// console.log(s[idx])

// console.log(s.charAt(idx))
// console.log(s.charCodeAt(3))

// console.log(s.includes('beqa'))

// console.log(s.replace('string', 'replaced')) // replace one
// console.log(s.replace(/string/gi, 'replaced')) // replace all

// console.log(s.split(' ').join('<--->'))

// console.log(s.startsWith('string'))
// console.log(s.endsWith('g'))


// console.log(s.substring(5, 7))

// console.log(s.toUpperCase())
// console.log(s.toLowerCase())

// console.log(s.indexOf('s')) // ეძებს პირველ შემხვედრს



// console.log(s.concat(' test')) // აერთიანებს სტრინგებს

// let c = ' string     '
// console.log(c)
// console.log(c.trim())


// let x = 'string';
// let y = '';
// // 'gnirts'

// for (let i = x.length - 1; i >= 0; i--) {
//     y += x[i]
//     // console.log(y);
// }

// console.log(y);


// let x = 'string 12';
// let y = 'string 45';
// let z = 456;
// console.log(`${x} ${y} ${z}`)

// let age = 24;
// console.log(`Your age is: ${age}`);