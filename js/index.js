
// ES 5
// var x = 5; //number
// var x1 = 'str \'asdasd asd asd asd asd "sdfsdf" ing'; //string
// var x2 = true // false //boolean
// var x3 = null // empty
// var x4 = undefined // empty

// var x5 = null;

// var x6;

// console.log(x5)
// console.log(x6)

// var x = 5;
// x = 10;

// const URL = 'www.instagram.com' // string


// ES 6+
// let x = 5;
// console.log(x)
// x = 10;
// console.log(x)


// const y = 10;
// y = 11;
// console.log(y)


// let PI = 3.14;

// let s = 'sxfsf sdjf bsjdf sjhf sd'

// let b = true;

// console.log(typeof b)


// let x = 10;
// let y = 20;

// console.log(x + y)
// console.log(x * y)
// console.log(x / y)
// console.log(x - y)
// console.log(19 % 7) // ნაშთი
// console.log(Math.sqrt(121))
// Math.pow(2, 4)
// console.log(2 ** 4) // power
// console.log(Math.pow(2, 4))

// let year = 2022;
// console.log('your age is: ', year - 1997)



// type coercion
// let x = 6;
// let y = 5;
// let z = '3'

// console.log(x + y + z)

// if (typeof z === 'number') {

// }


// let x = 100;
// x = x + ''


// let x = 5;
// x = x + 10;
// x += 10;

// x = x * 10;
// x *= 10;

// x -= 5;
// x /= 10;


// x = x + 1;
// x += 1;
// // alternative
// x++;


// x = x - 1;
// x -= 1;
// //alternative
// x--;

// console.log(x)

// const q = 10;
// q = // error




// multiline strings
// let x = `sdfsdf
// sxfsdfsdf
// sdf
// sdf
// sdf
// `;

// console.log(x)