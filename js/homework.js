// Homework 1
function func(array) {
    const numberArray = array.flat(Infinity).filter(function(item) {
        return typeof item === 'number'
    })
    
    if (numberArray.length === 0) {
        return 0;
    }

    return numberArray.reduce(function(a, b) {
        return a + b;
    });
}

// console.log(func(['1, 2, 3', [4, 5, 6]]));

// Homework 2
function func1(array) {
    let s = '';
    for (let i = 0; i < array.length; i++) {
        const item = array.slice(array.length - i - 1, array.length - i)[0];
        if (i == array.length - 1) {
            s += item;
        } else {
            s += item + ',';
        }
    }
    // console.log(s.split(',').map(function(x)  {
    //     return Number(x)
    // }))
    console.log(s.split(','));
}

func1([4, 2, 7, 1, -5, 12]);

// console.log(Number('5'))

// const arr = [4, 2, 7, 1, -5, 12];
// console.log(arr.slice(arr.length - 2, arr.length - 1))
