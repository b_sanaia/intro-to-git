// console.log('1');
// console.log('1');
// console.log('1');
// console.log('1');
// console.log('1');
// // heavy computation
// console.log('1');
// console.log('1');
// console.log('1');
// console.log('1');


// Execution block


// Event loop
// console.log('1');
// console.log('1');
// console.log('1');
// console.log('1');
// console.log('1');
// console.log('1');
// console.log('1');
// console.log('1');
// console.log('1');
// console.log('1');
// console.log('1');
// console.log('1');
// setTimeout(() => {
//     console.log('Timer ended')
// })
// console.log('2');


// 1                 //event loop
// 1                 // setTimeout
// 1
// 1
// 1
// 1
// 1
// 1
// 1
// 2
// setTimeout




// const x = 'string'; // pass by value
// const obj = {name: "Beqa"}; // pass by reference

// function test(a) {
//     a.name = 'Data';
//     return a;
// }


// test(obj);
// console.log(obj);




// Promises

// const promise = new Promise((resolve, reject) => {
//     console.log('In Promise');
//     resolve(1);
// });


// console.log(promise)

// promise.then((x) => {
//     console.log(x);
// })


// EX 2

// function async() {
//     return new Promise((resolve, reject) => {
//         console.log('Promise init');
//         setTimeout(() => {
//             console.log('Waiting for server response...');
//             const data = [{test: 1}, {test: 2}]
//             resolve(data)
//             // reject()
//         }, 2000)
//     })
// }


// const promise = async();
// console.log(promise);

// promise.then((data) => {
//     // redener in html
//     console.log(data);
//     console.log('Promise resolved')
// }, () => {
//     console.log('Server error.')
// });


// Promise all

// const promise1 = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         reject(1)
//     }, 2000)
// })

// const promise2 = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         resolve(2)
//     }, 1000)
// });

// Promise.all([promise1, promise2]).then(data => {
//     console.log(data);
// }, () => {
//     console.log('Error.')
// })

// Promise.race([promise1, promise2]).then(data => {
//     console.log(data);
// }, () => {
//     console.log('Error.')
// })



// Async and Await

// const func = async () => {
//     return 1;
// }

// const promise = func();
// console.log(promise);
// promise.then((x) => {
//     console.log(x);
// })


// const promise = async () => {
//     throw new Error('Server error.')
//     return 1;
// }

// promise().catch(error => {
//     console.log(error.message);
// })


// Await
// const returnData = async () => {
//     return 1;
// }

// const func = async () => {
//     console.log('in async function');
//     const data = await returnData();
//     console.log(data);
//     console.log('Async function finished');
// };

// func();


// const returnData = async () => {
//     // throw new Error('server error.');
//     return 1;
// }

// const func = async () => {
//     try {
//         const data = await returnData();
//         console.log(data);
//     } catch (error) {
//         console.log(error.message);
//     }
// }

// func()


// const delay = function(s) {
//     return new Promise((resolve, reject) => {
//         setTimeout(resolve, s*1000)
//     })
// }

// const getData = async () => {
//     await delay(2);
//     return 1;
// }

// const func = async() => {
//     const data = await getData();
//     console.log(data);
// }

// func();

// const one = async () => {
//     await delay(2);
//     console.log(1);
// }

// const two = async () => {
//     await delay(2);
//     console.log(2)
// }

// const func = async() => {
//     console.log('started...');
//     await one();
//     await two();
//     console.log('finished.');
// }

// func()


// Communication with server
// request and response

function getCountryInfo(countryName, isBorder) {
    const URL = `https://restcountries.com/v3.1/name/${countryName}`;
    fetch(URL).then(response => {
        return response.json();
    }).then(data => {
        if (!isBorder) {
            const borders = data[0].borders;
            renderCoutry(data[0]);
            for (let border of borders) {
                getCountryInfo(border, true);
            }
        } else {
            renderCoutry(data[0]);
        }
    })
}

function renderCoutry(countryData) {
    const img = countryData.flags.svg;
    const population = countryData.population;
    const capital = countryData.capital;
    const currency = Object.values(countryData.currencies)[0];

    const html = `
    <div class="country">
        <img id="flag" src="${img}" alt="">
        <p id="capital"><b>Capital: </b>${capital}</p>
        <p id="population"><b>Population: ${population}</b></p>
        <p id="currency"><b>Currency: ${currency.name} ${currency.symbol}</b></p>
    </div>`
    document.getElementById('countries').innerHTML += html;
}


getCountryInfo('china', false);